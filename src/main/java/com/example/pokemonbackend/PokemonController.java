package com.example.pokemonbackend;

import com.example.pokemonbackend.Pokemon;
import com.example.pokemonbackend.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PokemonController {

    @Autowired
    private PokemonService pokemonService;

    @GetMapping("/catch")
    public boolean catchPokemon() {
        return pokemonService.catchPokemon();
    }

    @GetMapping("/release")
    public int releasePokemon() {
        return pokemonService.releasePokemon();
    }

    @PostMapping("/rename")
    public String renamePokemon(@RequestParam String name, @RequestParam int sequence) {
        return pokemonService.renamePokemon(name, sequence);
    }

    @GetMapping("/pokemons")
    public List<Pokemon> getAllPokemons() {
        return pokemonService.getAllPokemons();
    }

    @PostMapping("/pokemons")
    public Pokemon savePokemon(@RequestBody Pokemon pokemon) {
        return pokemonService.savePokemon(pokemon);
    }

    @DeleteMapping("/pokemons/{id}")
    public void deletePokemon(@PathVariable Long id) {
        pokemonService.deletePokemon(id);
    }
}

