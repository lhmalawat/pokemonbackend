package com.example.pokemonbackend;

import com.example.pokemonbackend.Pokemon;
import com.example.pokemonbackend.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class PokemonService {

    @Autowired
    private PokemonRepository pokemonRepository;

    private List<String> fibonacciNicknames = new ArrayList<>();

    public PokemonService() {
        fibonacciNicknames.add("0");
        fibonacciNicknames.add("1");
    }

    public boolean catchPokemon() {
        return new Random().nextDouble() < 0.5;
    }

    public int releasePokemon() {
        int number = (new Random().nextInt(100)) + 1;
        return isPrime(number) ? number : -1;
    }

    public String renamePokemon(String originalName, int renameCount) {
        return originalName + "-" + getFibonacci(renameCount);
    }

    private boolean isPrime(int number) {
        if (number <= 1) return false;
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) return false;
        }
        return true;
    }

    private String getFibonacci(int count) {
        if (count < fibonacciNicknames.size()) {
            return fibonacciNicknames.get(count);
        }
        for (int i = fibonacciNicknames.size(); i <= count; i++) {
            int nextFib = Integer.parseInt(fibonacciNicknames.get(i - 1)) + Integer.parseInt(fibonacciNicknames.get(i - 2));
            fibonacciNicknames.add(String.valueOf(nextFib));
        }
        return fibonacciNicknames.get(count);
    }

    public List<Pokemon> getAllPokemons() {
        return pokemonRepository.findAll();
    }

    public Pokemon savePokemon(Pokemon pokemon) {
        return pokemonRepository.save(pokemon);
    }

    public void deletePokemon(Long id) {
        pokemonRepository.deleteById(id);
    }
}
